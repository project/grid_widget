<?php

namespace Drupal\grid_widget\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsButtonsWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'options_grid' widget.
 *
 * @FieldWidget(
 *   id = "options_grid",
 *   module = "grid_widget",
 *   label = @Translation("Options Grid"),
 *   field_types = {
 *     "entity_reference",
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *   },
 *   multiple_values = TRUE
 * )
 */
class OptionsGridWidget extends OptionsButtonsWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'property' => 'grid',
      'size' => 3,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['property'] = [
      '#type' => 'select',
      '#title' => $this->t('Layout with'),
      '#options' => $this->getPropertyOptions(),
      '#default_value' => $this->getSetting('property'),
      '#required' => TRUE,
    ];

    $element['size'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of columns'),
      '#default_value' => $this->getSetting('size'),
      '#required' => TRUE,
      '#min' => 2,
      '#max' => 8,
      '#step' => 1,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $property_options = $this->getPropertyOptions();

    $summary[] = $this->t('Layout: @property', ['@property' => ($property_options[$this->getSetting('property')]) ?? $property_options['grid']]);
    $summary[] = $this->t('Number of columns: @size', ['@size' => $this->getSetting('size')]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function form(FieldItemListInterface $items, array &$form, FormStateInterface $form_state, $get_delta = NULL) {
    $field_widget_complete_form = parent::form($items, $form, $form_state, $get_delta);
    $property_selected = $this->getSetting('property');

    if ($this->multiple) {
      $field_widget_complete_form['#attached']['library'][] = 'grid_widget/options_grid_checkboxes';
    }
    else {
      $field_widget_complete_form['#attached']['library'][] = 'grid_widget/options_grid_radios';
    }

    $field_widget_complete_form['#attributes']['class'][] = Html::getClass('field--widget-' . $this->getPluginId() . '--' . $property_selected);

    if ($property_selected === 'columns') {
      $field_widget_complete_form['#attributes']['data-columns-size'] = (int) $this->getSetting('size');
    }
    elseif ($property_selected === 'flex') {
      $field_widget_complete_form['#attributes']['data-flex-size'] = (int) $this->getSetting('size');
    }
    else {
      $field_widget_complete_form['#attributes']['data-grid-size'] = (int) $this->getSetting('size');
    }

    return $field_widget_complete_form;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    // Property to pass field's layout mode.
    $element['#options_grid'] = $this->getSetting('property') ?? 'grid';
    // Pass property as data attribute to reach more hooks/templates.
    $element['#attributes']['data-options-grid-item'] = $element['#options_grid'];

    return $element;
  }

  /**
   * Returns the options for the property.
   *
   * @return array
   *   List of options.
   */
  protected function getPropertyOptions() {
    return [
      'columns' => $this->t('Columns'),
      'flex' => $this->t('Flexbox'),
      'grid' => $this->t('Grid'),
    ];
  }

}
