CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

A complementary to Drupal core's Options module, Grid Widget defines a selection
checkboxes and radio buttons widget with styling options based on CSS grid
systems for text, numeric and entity reference fields.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/grid_widget

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/grid_widget


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the Grid Widget module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

* To use the field widget provided by this module, add a new List (text),
  List (float), List (integer), or Entity Reference field to s content type, for
  example at:
  Administration » Structure » Content Types » Basic Page » Manage Form Display

* "Options Grid" will be listed as an option in the field's Widget select.

* In the settings, you have the options to specify:
  - Layout with: The CSS grid system that will be the base of the styling of the
    form field layout. Options are: Columns, Flexbox, and Grid.
    Default value is Grid.
  - Number of columns: Define how many items per row will be the basis for the
    form field styling layout.
    Minimum value is 2.

MAINTAINERS
-----------

Current maintainers:
* Rob Little (LittleCoding) - https://www.drupal.org/user/1907742
